﻿using System;

namespace ToDo.MVC6.WebApi.Model
{
    public class ToDoItem
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public bool IsComlete { get; set; }
        public DateTime DateLimit { get; set; }
        public DateTime DateCompleted { get; set; }
    }
}
