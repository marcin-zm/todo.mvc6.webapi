﻿using System.Collections.Generic;

namespace ToDo.MVC6.WebApi.Model
{
    public interface IToDoRepository
    {
        IEnumerable<ToDoItem> GetAll();
        string Add(ToDoItem item);
        ToDoItem Remove(string key);
        ToDoItem Find(string key);
        void Completed(ToDoItem item);
    }
}
